
<?php
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
        <link rel="stylesheet" href="styles/main.css" />
        <link rel="stylesheet" href="css\reg.css" type="text/css">
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>
    </head>
    <body>
        <div class="reg">
        
        <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
        <h1>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Register with us</h1>
        <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
        <ul>
            <li>&emsp;Usernames may contain only digits, upper and lower case letters and underscores</li>
            <li>&emsp;Emails must have a valid email format</li>
            <li>&emsp;Passwords must be at least 6 characters long</li>
            <li>&emsp;Passwords must contain
                <ul>
                    <li>&emsp;At least one upper case letter (A..Z)</li>
                    <li>&emsp;At least one lower case letter (a..z)</li>
                    <li>&emsp;At least one number (0..9)</li>
                </ul>
            </li>
            <li>&emsp;Your password and confirmation must match exactly</li>
        </ul>
        <div class="text">
        <form method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
            &emsp;Username:<br> <input type='text' name='username' id='username' /><br>
            &emsp;Email:<br> <input type="text" name="email" id="email" /><br>
            &emsp;Password:<br> <input type="password"
                             name="password" 
                             id="password"/><br>
            &emsp;Confirm password:<br> <input type="password" 
                                     name="confirmpwd" 
                                     id="confirmpwd" /><br>
            <br>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type="button" 
                   value="Register" 
                   onclick="return regformhash(this.form,
                                   this.form.username,
                                   this.form.email,
                                   this.form.password,
                                   this.form.confirmpwd);" /> 
        </form>
        <p>&emsp;&emsp;Return to the <a href="index.php">login page</a>.</p>
    </div>
</div>
    </body>
</html>
